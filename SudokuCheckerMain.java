//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish and Alana Picozzi
// CMPSC 111 Spring 2017
// Lab #8
// Date: March 9th 2017
//
// Purpose: makin that sweet sudoku checker
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class SudokuCheckerMain

{

//----------------------------
// main method: program execution begins here
//----------------------------
public static void main(String[] args)

	{

// Label output with name and date:
System.out.println("Dylan Bish and Alana Picozzi \n Lab #8\n" + new Date() + "\n");
// Variable dictionary:
	int w1, w2 ,w3 ,w4, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;


	Scanner scan = new Scanner(System.in);

  	System.out.println("Please enter the first row of integers, separating each individual integer with the enter key. ");
  	w1 = scan.nextInt();
	w2 = scan.nextInt();
	w3 = scan.nextInt();
	w4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);

  	System.out.println("Please enter the second row of integers, separating each individual integer with the enter key. ");
  	x1 = scan.nextInt();
	x2 = scan.nextInt();
	x3 = scan.nextInt();
	x4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);
	System.out.println(x1 + " " + x2 + " " + x3 + " " + x4);

  	System.out.println("Please enter the third row of integers, separating each individual integer with the enter key. ");
  	y1 = scan.nextInt();
	y2 = scan.nextInt();
	y3 = scan.nextInt();
	y4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);
	System.out.println(x1 + " " + x2 + " " + x3 + " " + x4);
	System.out.println(y1 + " " + y2 + " " + y3 + " " + y4);

  	System.out.println("Please enter the fourth row of integers, separating each individual integer with the enter key. ");
  	z1 = scan.nextInt();
	z2 = scan.nextInt();
	z3 = scan.nextInt();
	z4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);
	System.out.println(x1 + " " + x2 + " " + x3 + " " + x4);
	System.out.println(y1 + " " + y2 + " " + y3 + " " + y4);
	System.out.println(z1 + " " + z2 + " " + z3 + " " + z4);



	int row1 = w1 + w2 + w3 + w4;
	int row2 = x1 + x2 + x3 + x4;
	int row3 = y1 + y2 + y3 + y4;
	int row4 = z1 + z2 + z3 + z4;

	int col1 = w1 + x1 + y1 + z1;
	int col2 = w2 + x2 + y2 + z2;
	int col3 = w3 + x3 + y3 + z3;
	int col4 = w3 + x3 + y3 + z3;


	int reg1 = w1 + w2 + x1 + x2;
	int reg2 = w3 + w4 + x3 + x4;
	int reg3 = y1 + y2 + z1 + z2;
	int reg4 = y3 + y4 + z3 + z4; 






	}

}
